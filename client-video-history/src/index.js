const express = require('express')
const passport = require('passport')
const {Strategy} = require('passport-google-oauth20')
const path = require('path')
const expressSession = require('express-session')
const redisStore = require('connect-redis')(expressSession)
const redis = require('redis')
const uuid = require('uuid')
const cors = require('cors')
require('dotenv').config()



const redisClient = redis.createClient(process.env.REDIS_PORT, process.env.REDIS_HOST)

redisClient.on('error', (err) => {
    console.log('Redis error: ', err)
})

const PORT = process.env.PORT || 3007

const config = {
    CLIENT_ID:process.env.CLIENT_ID,
    CLIENT_SECRET: process.env.CLIENT_SECRET
}

const AUTH_OPTIONS = {
    callbackURL: '/auth/google/callback',
    clientID: config.CLIENT_ID,
    clientSecret: config.CLIENT_SECRET
}

function verifyCallback(accessToken, refreshToken, params,  profile, done) {
    done(null, profile)
}
passport.use(new Strategy(AUTH_OPTIONS, verifyCallback))
passport.serializeUser((user, done) => {
    done(null, user)
})
passport.deserializeUser((obj, done) => {
    done(null, obj);
  });


const app = express()

app.use(expressSession({
    genid: (req) => {
        return uuid.v4()
    },
    store: new redisStore({host:process.env.REDIS_HOST, port:process.env.REDIS_PORT, client:redisClient}),
    name:'_redisDemo',
    secret: process.env.SESSION_SECRET,
    cookie: { secure: false, maxAge: 5 * 60 * 1000 }, // Set to secure:false and expire in 1 minute for demo purposes
    saveUninitialized: true
}))
app.use(passport.initialize())
app.use(passport.session())
app.use(cors())
//



// function checkLoggedIn(req, res, next) {
//     //console.log(req.sessionID, req.user)
//     console.log(req.sessionID)
//         redisClient.get(`sess:${req.sessionID}`, (err, data)=>{
//             try{
//             let sessionData = JSON.parse(data)
//             console.log(sessionData.passport.user._json)
//             if(sessionData.passport.user){
//                     if(sessionData.passport.user._json.email_verified){
//                         console.log('I am here 1')
//                         next(); 
//                     }
//                 }
//             }catch{
//                 return res.status(401).json({
//                     error: 'You must log in!'
//                 })
//             }
//         })
    
// }

app.get('/auth/google', passport.authenticate(  
    'google',
    {
        scope: ['email', 'profile']
    }
))

app.get('/auth/google/callback',(req, res, next) => {
    next()
}, passport.authenticate(
    'google', {
        failureRedirect:'/failure',
        successRedirect: '/',
        session: true
    }
))

app.get("/auth/logout", (req, res) => {
    req.logout()
    res.redirect('/')
})

app.get('/', (req, res, next) => {
    if(req.user){
        res.redirect(`http://127.0.0.1:3004/?userid=${req.sessionID}`)
    }else{
        next()
    }
    
})
app.use(express.static(path.join(__dirname,'..','public')), (req, res, next)=> {
    let pathName = path.join(__dirname, '..', 'public', 'index.html')
    res.sendFile(pathName)
})

app.get("/failure", (req, res) => {
    return res.send("Failed to login!");
});

app.listen(PORT, () => {
    console.log(`client-video-history app is listening on port ${PORT}`)
})







