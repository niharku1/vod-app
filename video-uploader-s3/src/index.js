const express = require('express')
const fs = require('fs')
const multer = require('multer')
const multerS3 = require('multer-s3')
const aws = require("aws-sdk");
const path = require('path');
const mongoose = require('mongoose')
const { Kafka, logLevel } = require('kafkajs')
const produceMessage = require('../service/producer.js')
const consumeMessage = require('../service/consumer.js');
const adminCreateTopic = require('../service/admin.js');

const topicName = 's3Upload'

//########################---admin----##########################
const kafkaAdmin = new Kafka({
  logLevel: logLevel.DEBUG,
  clientId: 'admin-1',
  brokers:['kafka1:9092', 'kafka2:9092','kafka3:9092'],
})

adminCreateTopic(kafkaAdmin, topicName)
//###########################--Producer--######################################
const kafka = new Kafka({
  logLevel: logLevel.DEBUG,
  clientId: 'producer-1',
  brokers:['kafka1:9092', 'kafka2:9092','kafka3:9092'],
})



const producer = kafka.producer()

const run = () => {
   produceMessage(producer)
}
run()

//###########################--Consumer--####################################
const kafkas = new Kafka({
  logLevel: logLevel.DEBUG,
  clientId: 'consumer-1',
  brokers:['kafka1:9092', 'kafka2:9092','kafka3:9092']
})
const consumer = kafkas.consumer({groupId: 'consumer-group'})
consumeMessage(kafkas, topicName, mongoose, consumer)

//#########################---MyApp- ###############################
const app = express()
const PORT = process.env.PORT
const s3 = new aws.S3({
    region:'ap-south-1',
    accessKeyId:process.env.AWS_ACCESS_KEY,
    secretAccessKey:process.env.AWS_SECRET_ACCESS_KEY
    
})

let fileName;
let imageName;
let upload = multer({
    storage: multerS3({
      s3: s3,
      bucket: `${process.env.BUCKET_NAME}`,
      acl: 'public-read',
      metadata: function (req, file, cb) {
        cb(null, {fieldName: file.fieldname});
      },
      key: function (req, file, cb) {
       
        if(file.fieldname === 'video') {
          const extension = file.originalname.split('.')[1]
          fileName = `${Date.now().toString()}.${extension}`
          cb(null, `videos/${fileName}`)
        }
        else{
          const extension = file.originalname.split('.')[1]
          imageName = `${Date.now().toString()}.${extension}`
          cb(null, `images/${imageName}`)
        }
        
      }
    })
  })


app.get('/', function (req, res) {
    let pathName = path.join(__dirname, '..', 'public','index.html')
    res.sendFile(pathName);
});

app.use(upload.fields([
  { name: "image", maxCount: 1 },
  { name: "video", maxCount: 1 }
]))
app.post("/upload",async function(req, res, next) {
    req.body['fileName'] = fileName
    req.body['imageName'] = imageName
    const msg = JSON.stringify(req.body)
    await producer.send({
      topic:topicName,
      messages: [{
        value:msg
      }]
    })
    
    res.json({
      msg:'Video uploaded successfully!',
      status:200
    })
    
})

app.listen(PORT, () => {
    console.log(`Microservice Online on port ${PORT}`)
})
