terraform {
  backend "s3" {
    bucket         = "vod-app-tfstate"
    key            = "vod-app.tfstate"
    region         = "ap-south-1"
    encrypt        = true
    dynamodb_table = "vod-app-tf-state-lock"
  }
}

provider "aws" {
  region  = "ap-south-1"
  version = "~> 2.54.0"
}

locals {
  prefix = "${var.prefix}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

resource "aws_s3_bucket" "app_public_files" {
  bucket        = local.prefix
  acl           = "public-read"
  force_destroy = true
}

resource "aws_s3_bucket_object" "object_1" {
  bucket       = aws_s3_bucket.app_public_files.id
  key          = "${var.videos}/"
  content_type = "application/x-directory"
}
resource "aws_s3_bucket_object" "object_2" {
  bucket       = aws_s3_bucket.app_public_files.id
  key          = "${var.images}/"
  content_type = "application/x-directory"
}