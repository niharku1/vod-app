variable "prefix" {
  description = "Name of s3 bucket"
}

variable "project" {
  default = "vod-app"
}

variable "contact" {
  default = "niharku@gmail.com"
}

variable "videos" {
  default = "videos"
}

variable "images" {
  default = "images"
}