output "bucket_name" {
  value = aws_s3_bucket.app_public_files.id
}

output "bucket_arn" {
  value = aws_s3_bucket.app_public_files.arn
}

output "bucket_information" {
  value = "bucket name: ${aws_s3_bucket.app_public_files.id}, bucket arn: ${aws_s3_bucket.app_public_files.arn}"
}