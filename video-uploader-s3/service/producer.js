const produceMessage = async (producer) => {
    try {
     
      await producer.connect()
      
      console.log('Producer connected successfully!')
    } catch (error) {
        console.log(error);
    }
}

module.exports = produceMessage