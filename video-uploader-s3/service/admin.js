const adminCreateTopic = async (kafka, topic) => {
    const admin = kafka.admin()
    await admin.connect()
    await admin.createTopics({
        waitForLeaders: true,
        topics:[{
            topic: topic,
            numPartitions: 3,
            replicationFactor: 3
        }]
    })
    await admin.disconnect()
}

module.exports = adminCreateTopic