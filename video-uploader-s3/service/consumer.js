const consumeMessage = async (kafkas, topic, mongoose, consumer) => {
    const connect = () => {
        return mongoose.connect(`${process.env.DBHOST}/${process.env.DBNAME}`)
      }
    const videoSchema = new mongoose.Schema({})
    const videosCollection = mongoose.model('myModel', videoSchema, 'videos')
    // Consuming
    await consumer.connect() //connecting to consumer created above
    await consumer.subscribe({ topic }) //Consumer subscribing to a topic
    await consumer.run({
      eachMessage: async ({ topic, partition, message }) => {
        let msg = message.value.toString()
        msg = JSON.parse(msg)
          if(message.value){
            connect().then(async() => {
                const video_props = await videosCollection.collection.insertOne(msg)
                console.log('video uploaded successfully!')
           }).catch()
          }
      }
    })
  }

  module.exports = consumeMessage