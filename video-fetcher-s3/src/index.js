const express = require('express')
const fs = require('fs')
const { S3Client, GetObjectCommand, ListObjectsCommand } = require("@aws-sdk/client-s3");

const app = express()

const PORT = process.env.PORT

const s3 = new S3Client({
    region:'ap-south-1',
    credentials: {
        accessKeyId:process.env.AWS_ACCESS_KEY,
        secretAccessKey:process.env.AWS_SECRET_ACCESS_KEY
    }
})

app.get("/video", async(req, res) => {
    try{
        const getParams = {
            Bucket:`${process.env.BUCKET_NAME}`,
            Key:`videos/${req.query.path}`,
            Prefix: `videos/${req.query.path}`
        }
       
        const metadata = await s3.send(new ListObjectsCommand(getParams))
        const contentLength = metadata.Contents[0].Size
        const data = await s3.send(new GetObjectCommand(getParams))
        res.writeHead(200, {
            "Content-Length":contentLength,
            "Content-Type": "video/mp4"
        })
        data.Body.pipe(res)
    }catch(e){
        res.sendStatus(500);
        return;
    }

})

app.listen(PORT, () => {
    console.log(`Microservice Online on port ${PORT}`)
})
