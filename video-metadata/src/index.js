const express = require('express')
const mongoose = require('mongoose')

const app = express()
const PORT = process.env.PORT
const DBNAME = process.env.DBNAME
const DBHOST = process.env.DBHOST


const connect = () => {
    return mongoose.connect(`${DBHOST}/${DBNAME}`)
}
const videoSchema = new mongoose.Schema({})
const videosCollection = mongoose.model('myModel', videoSchema, 'videos')

app.get('/metadata', (req, res) => {
    connect().then(async() => {
        const video_id = req.query.id
        const videoRecord = await videosCollection.collection.findOne({fileName:video_id})
        if(!videoRecord) {
            res.sendStatus(404)
            return;
        }
       res.send(videoRecord)
    })
    

})

app.listen(PORT, () => {
    console.log(`video-metadata app is listening on port ${PORT}`)
})