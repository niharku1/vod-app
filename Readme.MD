<h1>Video On Demand-APP</h1>
<br/>
<h4>Summary:</h4>
<h6>Technologies Used:</h6>
<ul>
<li><b>Javascript</b></li>
<li><b>ReactJS</b></li>
<li><b>Redux</b></li>
<li><b>Express.JS</b></li>
<li><b>Node.JS</b></li>
<li><b>MongoDB</b></li>
<li><b>Redis</b></li>
<li><b>Oauth</b></li>
<li><b>Apache Kafka</b></li>
<li><b>Terraform</b></li>
</ul>
<br/>
<h6>Design Pattern Used:</h6>
<ul><li>Microservices</li>
<li>Pub-Sub pattern using Apache Kafka in video-uploader microservice</li>
</ul>
<br/>
<h6>Working:</h6>
<p>This app uses streams to stream videos from S3 bucket to the client screen using Node.JS. Following microservices have been created to achieve this task:
<ul>
<li><b>video-uploader-s3</b>: This microservice is used to upload videos and their respective images to the s3 bucket using multerS3 package. We should open this app first at <b>localhost:3003</b> and upload videos. This app stores the video metadata into the MongoDB collection using Apache Kafka.</li>
<li><b>video-streaming</b>: This microservice is used to stream videos to the client screen. It first gets video metadata from MongoDB using <b>video-metadata</b> microservice and then uses <b>video-fetcher-s3</b> microservice to fetch video from S3 bucket which we uploaded using above microservice. It then displays streams of videos on client screen using <b>video-displayer</b> microservice inside a video Tag using ReactJS.</li>
<li><b>client-video-history</b>: This micro service stores the client data like watchTime, video name, login details (implemented using oauth) etc. It also creates clients session inside Redis with a session time of 5 minutes, after which session is destroyed and client has to login again.</li>
</ul></p>
<br/>
<h5>
<h6>How to run the app?</h6>
<pre>
Step1: Create an AWS account and then give administrator access to the IAM user.

Step2: Create S3 bucket "vod-app-tfstate" and dynamodb table with name "vod-app-tf-state-lock" for storing tfstate and creating a link to that tfstate resp using AWS Dashboard.

Step3: Store the following environment variables using <b>sudo -H gedit /etc/environment</b> command:
        1. AWS_ACCESS_KEY_ID="AxxxxxxxxxxxxxxxxxxZ"
        2. AWS_SECRET_ACCESS_KEY="7xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxR"
        3. CLIENT_ID="5xxxxxxxxxxxxxxxxxxxxxxxxx.com"
        4. CLIENT_SECRET="Gxxxxxxxxxxxxxxxxxxxxxxxxxxxxxw"
        5. SESSION_SECRET="vxxxxxxxxxxxxxxxxxxxxxo"
        6. DB_HOST="mxxxxxxxxxxxxxxxxxxx7"
        7. BUCKET_NAME="vxxxxxxxxxxxxxxn" (should be same as in terraform.tfvars)

Step4: Run Following commands:
       a. sudo docker-compose -f video-uploader-s3/deployTF/docker-compose.yml run --rm tf init
       b. sudo docker-compose -f video-uploader-s3/deployTF/docker-compose.yml run --rm tf fmt
       c. sudo docker-compose -f video-uploader-s3/deployTF/docker-compose.yml run --rm tf validate
       d. sudo docker-compose -f video-uploader-s3/deployTF/docker-compose.yml run --rm tf plan
       e. sudo docker-compose -f video-uploader-s3/deployTF/docker-compose.yml run --rm tf apply
       f. sudo docker-compose -f video-uploader-s3/deployTF/docker-compose.yml run --rm tf destroy

Step4: Run sudo docker-compose up --build
Step5: Go to localhost:3003 and upload Videos along with their images. Some of the videos have been provided in the files directory along with images, use any combination of   images and videos. Also videos should be in .mp4 format.
Step6: Go to localhost:3005 and login and you will be redirected to video page after successful login and then watch your uploaded videos.
</pre>
</h5>
<h5>Desired Improvements and Upcoming Features:</h5>
<ul>
<li>A video-recommendation microservice can be added to present stack to recommend videos to the client based on genre they like to watch, or the actor they like to watch etc.</li>
<li>This App can be modified more to look more professional. Currently Work is being done on this part.</li>
<li>Videos can be gzipped using multers3 to make uploads and downloads of video faster from s3.</li>
</ul>