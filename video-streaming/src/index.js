const express = require('express')
const http = require('http')
const path = require('path')

const app = express()
const PORT = process.env.PORT
const VIDEO_METADATA_HOST = process.env.VIDEO_METADATA_HOST
const VIDEO_METADATA_PORT = process.env.VIDEO_METADATA_PORT
const VIDEO_FETCHER_HOST = process.env.VIDEO_FETCHER_HOST
const VIDEO_FETCHER_PORT = process.env.VIDEO_FETCHER_PORT


app.get('/video', (req, res) => {
    const metadata_req = http.request({
        host:VIDEO_METADATA_HOST,
        port:VIDEO_METADATA_PORT,
        path:`/metadata?id=${req.query.id}`,
        method: 'GET'
    }, (resp) => {
        resp.on('data', (chunk) => {
            if (chunk.toString().toLowerCase() !== 'not found'){
                const videoPath = JSON.parse(chunk.toString()).fileName //contains video metadata
                const video_fetch_req = http.request({
                    host:VIDEO_FETCHER_HOST,
                    port:VIDEO_FETCHER_PORT,
                    path:`/video?path=${videoPath}`,
                    method: 'GET',
                    headers: req.headers
                },
                (response) => {
                    response.headers["Accept-Ranges"] = "bytes"
                    res.writeHeader(response.statusCode, response.headers)
                    response.pipe(res)
                })
                req.pipe(video_fetch_req)

            }
        })
    })
    req.pipe(metadata_req)
    
})

app.listen(PORT, () => {
    console.log(`video-streaming service is listening at port ${PORT}`)
})