function urlAction(url) {
    return{
        type:'updateUrl',
        payload:{
            url
        }
    }
}
export default urlAction