function metadataAction(videoMetadata) {
    return{
        type:'updateMetadata',
        payload:{
            videoMetadata
        }
    }
}
export default metadataAction