import React, {Component} from "react";
import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'
import urlAction from "./action/urlAction";
import axios from 'axios'

class Player extends Component {
    constructor(){
        super()
        this.state = {user:{}, propData:{}}
    }
    componentDidMount(){
        if(localStorage.getItem('userData') !== null){
            const userData = JSON.parse(localStorage.getItem('userData'))
            const videoId = this.props.url.url.split("=")[1].split(".")[0]
            const extension = this.props.url.url.split("=")[1].split(".")[1]
            if(userData.id !== null && videoId in userData) {
                document.getElementById('video').currentTime = userData[videoId][extension].currentTime
            }
        }
        else if(Object.keys(this.state.user).length === 0){
            const url = JSON.parse(localStorage.getItem("properties"))
            axios.get(`http://127.0.0.1:3004/userMetadata?userid=${url.userid}`).then((response)=> {
                console.log(response)
                let userData = response.data
                const videoId = this.props.url.url.split("=")[1].split(".")[0]
                const extension = this.props.url.url.split("=")[1].split(".")[1]
                if(videoId in userData) {
                    document.getElementById('video').currentTime = userData[videoId][extension].currentTime
                }
                this.setState({
                    user:userData
                })
            })
            
    }
    }
    componentWillUnmount(){
        const video = document.getElementById('video')
        const currentTime = video.currentTime
        const totalDuration = video.duration
        const videoId = video.currentSrc.split("=")[1].split(".")[0]
        const extension = video.currentSrc.split("=")[1].split(".")[1]
        const user = {...this.state.user}
        const properties = JSON.parse(localStorage.getItem("properties"))
        if(localStorage.getItem('userData') === null){
            user[videoId] = {}
            user[videoId][extension] = {
                currentTime:currentTime,
                name:properties.name,
                genre:properties.genre,
                actor:properties.actor,
                duration:totalDuration
            }
            console.log(user)
            localStorage.removeItem('properties')
            localStorage.setItem('userData', JSON.stringify(user))
        }else{
            const userData = JSON.parse(localStorage.getItem('userData'))
            console.log(userData)
            if(videoId in userData){
                userData[videoId][extension]["currentTime"] = currentTime
                localStorage.removeItem('properties')
                localStorage.setItem('userData', JSON.stringify(userData))
            }else{
                userData[videoId] = {}
                userData[videoId][extension] = {
                    currentTime:currentTime,
                    name:properties.name,
                    genre:properties.genre,
                    actor:properties.actor,
                    duration:totalDuration
                }
            localStorage.removeItem('properties')
            localStorage.setItem('userData', JSON.stringify(userData))
            }
        }
        
    }
    render(){
        return(
            <video width="1280" height="720" controls id='video'>
                <source src={this.props.url.url} type="video/mp4"/>   
            </video>
        )
    }
}

function mapStateToProps(state){
    return {
        url: state.url
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        urlAction:urlAction
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Player)