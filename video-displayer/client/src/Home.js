import React, {Component} from "react";
import { Link } from 'react-router-dom';
import axios from 'axios'
import Player from "./Player";
import { connect } from "react-redux";
import { bindActionCreators } from 'redux'
import urlAction from "./action/urlAction";
import metadataAction from "./action/metadataAction"

class Home extends Component {

  constructor() {
    super()
    this.userid = window.location.search.split("=")[1]
  }
  static getDerivedStateFromProps(){
    console.log('I am here')
    if(localStorage.getItem('userData') !== null){
      const user = JSON.parse(localStorage.getItem('userData'))
      axios.post('http://127.0.0.1:3004/updateUserData',user).then((response) => {
        console.log(response.data.value)
        })
    }
    
  }
  componentDidMount(){
    console.log('I am here')
    const videos_metadata_url = "http://127.0.0.1:3004/getAllVideosMetadata"
    axios.get(videos_metadata_url).then((resp) => {
      console.log(resp.data)
      const videos_metadata = resp.data
      this.props.metadataAction(videos_metadata)
    }).catch()
  }
  

  onClickHandler(url){
    this.props.urlAction(url)
  }


  render(){
    const videosUrl = "http://127.0.0.1:3000/video?id="
    const imgUrl = `https://${process.env.REACT_APP_BUCKET_NAME}.s3.ap-south-1.amazonaws.com/images/`
    let imageGrid;
    console.log(this.props, this.props.metadata)
    if(this.props.metadata.length !==0 && typeof this.props.metadata !== 'string'){
       imageGrid = this.props.metadata.map((videos, index) => {
        let properties = {
          name:videos.name,
          genre:videos.genre,
          actor:videos.actor,
          userid: this.userid
        }
        localStorage.setItem("properties", JSON.stringify(properties));
        return( <div className="col s3" key={index} onClick={() => this.onClickHandler(`${videosUrl}${videos.fileName}`)}>
             <Link to={{
               pathname:'/player',
             }}>
               <img src={`${imgUrl}${videos.imageName}`} />
            </Link>
        </div>
        )
       })
    }
    
    return(
      <div className="row">
          {imageGrid}
      </div>
    )
  }
}

function mapStateToProps(state){
  return{
    url:state.url,
    metadata: state.metadata
  }
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({
      urlAction:urlAction,
      metadataAction:metadataAction
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
