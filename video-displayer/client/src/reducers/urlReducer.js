function urlReducer(state={url:''}, action){
    if(action.type === 'updateUrl') {
        let newState = {...state}
        if(action.payload.url){
            newState['url'] = action.payload.url
        }
        return newState
    }
    return state
}

export default urlReducer