import { combineReducers } from "redux";
import urlReducer from "./urlReducer";
import metadataReducer from "./metadataReducer";

const rootReducer = combineReducers({
    url: urlReducer,
    metadata: metadataReducer

})

export default rootReducer