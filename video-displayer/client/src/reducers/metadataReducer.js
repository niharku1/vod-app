function metadataReducer(state=[], action){
    if(action.type === 'updateMetadata') {
        let newState = [...state]
        if(action.payload.videoMetadata){
            newState = action.payload.videoMetadata
        }
        return newState
    }
    return state
}

export default metadataReducer