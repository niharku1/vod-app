import React, { Component, lazy, Suspense } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './App.css';

import Home from './Home';
import Player from './Player';

class App extends Component{

	render(){
    	return(
      		<Router>
					<Route exact path="/" component={Home} />
					<Route exact path="/player" component={Player} />
      		</Router>
    	)
  	}

}




export default App