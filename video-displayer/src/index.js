const express = require('express')
const mongoose = require('mongoose')
const path = require('path')
const redis = require('redis')
const morgan = require('morgan')
const redisClient = redis.createClient(process.env.REDIS_PORT, process.env.REDIS_HOST)

redisClient.on('error', (err) => {
    console.log('Redis error: ', err)
})


const app = express()
const router = express.Router()
const PORT = process.env.PORT
const connect = () => {
    return mongoose.connect(`${process.env.DBHOST}/${process.env.DBNAME}`)
}

const videoSchema = new mongoose.Schema({})
const videosCollection = mongoose.model('myModel', videoSchema, 'videos')
const userSchema = new mongoose.Schema({})
const userCollection = mongoose.model('myModel1', userSchema, 'users')

router.get('/', (req, res, next) => {
    let userId = req.query.userid
    redisClient.get(`sess:${userId}`, (err, data)=>{
        
        const sessionData = JSON.parse(data)
        if(sessionData !== null){
            next(sessionData.passport.user)
        }else{
            res.redirect('http://localhost:3005/auth/logout')
        }
    })
}, (data, req, res, next) => {
    connect().then(async(connection)=> {
        const userAlreadyexistsData = await userCollection.collection.findOne({
            id: data.id
        })
        if(!userAlreadyexistsData){
            const userData = await userCollection.collection.insertOne(data)
            if(userData){
                req['user'] = userData
                next()
            }
    }
    next()
        
    })
})

router.get('/player', (req, res) => {
    let pathname = path.join(__dirname, '..', 'public', 'index.html')
    res.sendFile(pathname)
})
router.get('/getAllVideosMetadata', (req, res) => {
    connect().then(async()=> {
        const videos_metadata = await videosCollection.find({})
        res.send(videos_metadata)
    }).catch()
})

router.get('/userMetadata', (req, res) => {
    let sessionId = req.query.userid
    redisClient.get(`sess:${sessionId}`, (err, data)=>{
        const sessionData = JSON.parse(data)
        const user_id = sessionData.passport.user.id
        connect().then(async() => {
            const userData = await userCollection.collection.findOne({id:user_id})
            res.send(userData)
        }).catch()
        
    })
})

router.post('/updateUserData', (req, res) => {
    connect().then(async()=> {
        delete req.body._id
        const user_metadata = await userCollection.collection.findOneAndUpdate({id:req.body.id}, {$set:req.body}, {upsert:true})
        res.send(user_metadata)
    }).catch()
    
})
app.use(morgan('combined'))
app.use(express.urlencoded({ extended: true }))
app.use(express.json())
app.use(router)
app.use('/', express.static(path.join(__dirname, '..', 'public')), (req, res)=>{
    let pathname = path.join(__dirname, '..', 'public', 'index.html')
    res.sendFile(pathname)
})

app.listen(PORT, () => {
    console.log(`video-displayer service is listening on port ${PORT}`)
})